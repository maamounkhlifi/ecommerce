﻿using Entities.DAL;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Contracts.Repositories
{
    public interface ICustomersRepository : IRepositoryBase<Customers>
    {
        Task<bool> Exists(int id);
    }
}
