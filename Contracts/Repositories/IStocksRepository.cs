﻿using Entities.DAL;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Contracts.Repositories
{
    public interface IStocksRepository : IRepositoryBase<Stocks>
    {
        Task<bool> Exists(int productId, int storeId);
    }
}
