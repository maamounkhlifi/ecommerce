﻿using Entities.DAL;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Contracts.Repositories
{
    public interface IProductsRepository : IRepositoryBase<Products>
    {
        Task<bool> Exists(int id);
    }
}
