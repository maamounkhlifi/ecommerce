﻿using Entities.DAL;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Contracts.Repositories
{
    public interface IOrderItemsRepository : IRepositoryBase<OrderItems>
    {
        Task<bool> Exists(int orderId, int ItemId);
    }
}
