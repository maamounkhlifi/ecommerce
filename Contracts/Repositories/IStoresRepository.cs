﻿using Entities.DAL;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Contracts.Repositories
{
    public interface IStoresRepository : IRepositoryBase<Stores>
    {
        Task<bool> Exists(int id);
    }
}
