﻿using System;
using Entities.DAL;
using System.Collections.Generic;
using System.Text;
using Entities.DTO;
using System.Threading.Tasks;

namespace Contracts.Services
{
    public interface IOrdersService : IServiceBase<Orders, OrdersDTO>
    {
        Task<bool> Exists(int id);
    }
}
