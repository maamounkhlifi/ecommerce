﻿using Entities.DAL;
using Entities.DTO;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Contracts.Services
{
    public interface IStocksService : IServiceBase<Stocks, StocksDTO>
    {
        Task<bool> Exists(int productId, int storeId);
    }
}
