﻿using Entities.DAL;
using Entities.DTO;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Contracts.Services
{
    public interface ICustomersService : IServiceBase<Customers, CustomersDTO>
    {
        Task<bool> Exists(int id);
    }
}
