﻿using Entities.DAL;
using Entities.DTO;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Contracts.Services
{
    public interface IProductsService : IServiceBase<Products, ProductsDTO>
    {
        Task<bool> Exists(int id);
    }
}
