﻿using Entities.DAL;
using Entities.DTO;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Contracts.Services
{
    public interface IOrderItemsService : IServiceBase<OrderItems, OrderItemsDTO>
    {
        Task<bool> Exists(int orderId, int ItemId);
    }
}
