﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Contracts.Services
{
    public interface IServiceBase<TSource, TDestination>
    {
        IQueryable<TDestination> FindAll();
        IEnumerable<TDestination> FindAllAsync();
        IQueryable<TDestination> FindByCondition(Expression<Func<TSource, bool>> expression);
        IEnumerable<TDestination> FindByConditionAsync(Expression<Func<TSource, bool>> expression);
        IEnumerable<TDestination> FindByConditionAsync(Expression<Func<TSource, bool>> expression, params Expression<Func<TSource, object>>[] includeExpressions);
        void Create(TDestination entity);
        void Update(TDestination entity);
        void Delete(TDestination entity);
        Task Save();
    }
}
