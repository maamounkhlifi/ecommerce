﻿using AutoMapper;
using Contracts.Repositories;
using Contracts.Services;
using Entities.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public class ServiceBase<TSource, TDestination> : IServiceBase<TSource, TDestination>
    {
        internal readonly IRepositoryBase<TSource> _repository;
        internal readonly ecommerceContext _context;
        internal readonly IMapper _mapper;

        public ServiceBase(IRepositoryBase<TSource> repository, ecommerceContext ecommerceContext, IMapper mapper)
        {
            _repository = repository;
            _context = ecommerceContext;
            _mapper = mapper;
        }

        public void Create(TDestination entityDTO)
        {
            var entity = _mapper.Map<TDestination, TSource>(entityDTO);
            _repository.Create(entity);
        }

        public void Delete(TDestination entityDTO)
        {
            var entity = _mapper.Map<TDestination, TSource>(entityDTO);
            _repository.Delete(entity);
        }

        public IQueryable<TDestination> FindAll()
        {
            var result = _repository.FindAll();
            return result.Select(p => _mapper.Map<TSource, TDestination>(p));

        }

        public IEnumerable<TDestination> FindAllAsync()
        {
            var result = _repository.FindAllAsync();
            return result.Result.Select(p => _mapper.Map<TSource, TDestination>(p)).AsEnumerable();
             
        }

        public IQueryable<TDestination> FindByCondition(Expression<Func<TSource, bool>> expression)
        {
            var result = _repository.FindByCondition(expression);

            return result.Select(p => _mapper.Map<TSource, TDestination>(p));
        }

        public IEnumerable<TDestination> FindByConditionAsync(Expression<Func<TSource, bool>> expression)
        {
            var result = _repository.FindByConditionAsync(expression);
            return result.Result.Select(p => _mapper.Map<TSource, TDestination>(p)).AsEnumerable();
        }

        public IEnumerable<TDestination> FindByConditionAsync(Expression<Func<TSource, bool>> expression, params Expression<Func<TSource, object>>[] includeExpressions)
        {
            var result = _repository.FindByConditionAsync(expression, includeExpressions);
            return result.Result.Select(p => _mapper.Map<TSource, TDestination>(p)).AsEnumerable();
        }

        public void Update(TDestination entityDTO)
        {
            var entity = _mapper.Map<TDestination, TSource>(entityDTO);
            _repository.Update(entity);
        }

        public async Task Save()
        {
            await _context.SaveChangesAsync();
        }
       
    }
}
