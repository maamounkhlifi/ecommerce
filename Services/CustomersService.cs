﻿using AutoMapper;
using Contracts.Repositories;
using Contracts.Services;
using Entities.DAL;
using Entities.DTO;
using Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public class CustomersService : ServiceBase<Customers, CustomersDTO>, ICustomersService
    {
        ICustomersRepository _customersRepository;

        public CustomersService(ICustomersRepository repository, ecommerceContext ecommerceContext, IMapper mapper)
            : base(repository, ecommerceContext, mapper)
        {
            _customersRepository = repository;
        }

        public Task<bool> Exists(int id)
        {
            return this._customersRepository.Exists(id);
        }

    }
}
