﻿using AutoMapper;
using Contracts.Repositories;
using Contracts.Services;
using Entities.DAL;
using Entities.DTO;
using Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public class CategoriesService : ServiceBase<Categories, CategoriesDTO>, ICategoriesService
    {
        ICategoriesRepository _categoriesRepository;

        public CategoriesService(ICategoriesRepository repository, ecommerceContext ecommerceContext, IMapper mapper)
            : base(repository, ecommerceContext, mapper)
        {
            _categoriesRepository = repository;
        }

        public Task<bool> Exists(int id)
        {
            return this._categoriesRepository.Exists(id);
        }

    }
}
