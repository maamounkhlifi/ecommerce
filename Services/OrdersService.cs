﻿using AutoMapper;
using Contracts.Repositories;
using Contracts.Services;
using Entities.DAL;
using Entities.DTO;
using Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public class OrdersService : ServiceBase<Orders, OrdersDTO>, IOrdersService
    {
        IOrdersRepository _ordersRepository;

        public OrdersService(IOrdersRepository repository, ecommerceContext ecommerceContext, IMapper mapper)
            : base(repository, ecommerceContext, mapper)
        {
            _ordersRepository = repository;
        }

        public Task<bool> Exists(int id)
        {
            return this._ordersRepository.Exists(id);
        }

    }
}
