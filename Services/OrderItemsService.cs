﻿using AutoMapper;
using Contracts.Repositories;
using Contracts.Services;
using Entities.DAL;
using Entities.DTO;
using Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public class OrderItemsService : ServiceBase<OrderItems, OrderItemsDTO>, IOrderItemsService
    {
        IOrderItemsRepository _orderItemsRepository;

        public OrderItemsService(IOrderItemsRepository repository, ecommerceContext ecommerceContext, IMapper mapper)
            : base(repository, ecommerceContext, mapper)
        {
            _orderItemsRepository = repository;
        }

        public Task<bool> Exists(int orderId, int ItemId)
        {
            return this._orderItemsRepository.Exists(orderId, ItemId);
        }
    }
}
