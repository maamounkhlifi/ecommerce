﻿using AutoMapper;
using Contracts.Repositories;
using Contracts.Services;
using Entities.DAL;
using Entities.DTO;
using Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public class StoresService : ServiceBase<Stores, StoresDTO>, IStoresService
    {
        IStoresRepository _storesRepository;

        public StoresService(IStoresRepository repository, ecommerceContext ecommerceContext, IMapper mapper)
            : base(repository, ecommerceContext, mapper)
        {
            _storesRepository = repository;
        }

        public Task<bool> Exists(int id)
        {
            return this._storesRepository.Exists(id);
        }

    }
}
