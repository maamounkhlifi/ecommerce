﻿using AutoMapper;
using Contracts.Repositories;
using Contracts.Services;
using Entities.DAL;
using Entities.DTO;
using Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public class StocksService : ServiceBase<Stocks, StocksDTO>, IStocksService
    {
        IStocksRepository _stocksRepository;

        public StocksService(IStocksRepository repository, ecommerceContext ecommerceContext, IMapper mapper)
            : base(repository, ecommerceContext, mapper)
        {
            _stocksRepository = repository;
        }

        public Task<bool> Exists(int productId, int storeId)
        {
            return this._stocksRepository.Exists(productId, storeId);
        }

    }
}
