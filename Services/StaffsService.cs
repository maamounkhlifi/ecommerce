﻿using AutoMapper;
using Contracts.Repositories;
using Contracts.Services;
using Entities.DAL;
using Entities.DTO;
using Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public class StaffsService : ServiceBase<Staffs, StaffsDTO>, IStaffsService
    {
        IStaffsRepository _staffsRepository;

        public StaffsService(IStaffsRepository repository, ecommerceContext ecommerceContext, IMapper mapper)
            : base(repository, ecommerceContext, mapper)
        {
            _staffsRepository = repository;
        }

        public Task<bool> Exists(int id)
        {
            return this._staffsRepository.Exists(id);
        }
    }
}
