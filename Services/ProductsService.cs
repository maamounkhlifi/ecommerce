﻿using AutoMapper;
using Contracts.Repositories;
using Contracts.Services;
using Entities.DAL;
using Entities.DTO;
using Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public class ProductsService : ServiceBase<Products, ProductsDTO>, IProductsService
    {
        IProductsRepository _productsRepository;

        public ProductsService(IProductsRepository repository, ecommerceContext ecommerceContext, IMapper mapper)
            : base(repository, ecommerceContext, mapper)
        {
            _productsRepository = repository;
        }

        public Task<bool> Exists(int id)
        {            
            return this._productsRepository.Exists(id);
        }
    }
}
