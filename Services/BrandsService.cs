﻿using AutoMapper;
using Contracts.Repositories;
using Contracts.Services;
using Entities.DAL;
using Entities.DTO;
using Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Services
{    
    public class BrandsService : ServiceBase<Brands, BrandsDTO>, IBrandsService
    {
        IBrandsRepository _brandsRepository;

        public BrandsService(IBrandsRepository repository, ecommerceContext ecommerceContext, IMapper mapper)
            : base(repository, ecommerceContext, mapper)
        {
            _brandsRepository = repository;
        }

        public Task<bool> Exists(int id)
        {
            return this._brandsRepository.Exists(id);
        }

    }
}
