﻿using Models;
using System;
using System.Collections.Generic;

namespace Entities.DTO
{
    public class BrandsDTO : IDTO
    {
        public int BrandId { get; set; }
        public string BrandName { get; set; }
    }

    public partial class Brand
    {
        public string BrandName { get; set; }
    }
}
