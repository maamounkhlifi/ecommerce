﻿using Models;
using System;
using System.Collections.Generic;

namespace Entities.DTO
{
    public class StaffsDTO : IDTO
    {
        public int StaffId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public byte Active { get; set; }
        public int StoreId { get; set; }
        public int? ManagerId { get; set; }

        public Staff Manager { get; set; }        
        public Store Store { get; set; }
       
    }

    public partial class Staff
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        //public byte Active { get; set; }
        //public int? ManagerId { get; set; }

    }
}
