﻿using Models;
using System;
using System.Collections.Generic;

namespace Entities.DTO
{
    public class StocksDTO : IDTO
    {
        public int StoreId { get; set; }
        public int ProductId { get; set; }
        public int? Quantity { get; set; }

        public Product Product { get; set; }
        public Store Store { get; set; }
    }
}
