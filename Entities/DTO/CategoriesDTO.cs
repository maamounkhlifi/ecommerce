﻿using Models;
using System;
using System.Collections.Generic;

namespace Entities.DTO
{
    public class CategoriesDTO : IDTO
    {
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }

    }

    public partial class Category
    {
        public string CategoryName { get; set; }
    }
}
