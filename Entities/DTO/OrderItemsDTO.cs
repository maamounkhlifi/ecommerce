﻿using Models;
using System;
using System.Collections.Generic;

namespace Entities.DTO
{
    public class OrderItemsDTO : IDTO
    {
        public int OrderId { get; set; }
        public int ItemId { get; set; }
        public int ProductId { get; set; }
        public int Quantity { get; set; }
        public decimal ListPrice { get; set; }
        public decimal Discount { get; set; }

        public Order Order { get; set; }
        public Product Product { get; set; }
    }        
}
