﻿using Models;
using System;
using System.Collections.Generic;

namespace Entities.DTO
{
    public class OrdersDTO : IDTO
    {
        public int OrderId { get; set; }
        public int? CustomerId { get; set; }
        public byte OrderStatus { get; set; }
        public DateTime OrderDate { get; set; }
        public DateTime RequiredDate { get; set; }
        public DateTime? ShippedDate { get; set; }
        public int StoreId { get; set; }
        public int StaffId { get; set; }

        public Customer Customer { get; set; }
        public Staff Staff { get; set; }
        public Store Store { get; set; }
    }

    public partial class Order
    {
        //public int? OrderCustomerId { get; set; }
        public byte OrderStatus { get; set; }
        public DateTime OrderDate { get; set; }
        public DateTime RequiredDate { get; set; }
        public DateTime? ShippedDate { get; set; }
        //public int StoreId { get; set; }
        //public int StaffId { get; set; }
    }
}
