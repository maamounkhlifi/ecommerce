﻿using Models;
using System;
using System.Collections.Generic;

namespace Entities.DTO
{
    public class ProductsDTO : IDTO
    {
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public int BrandId { get; set; }
        public int CategoryId { get; set; }
        public short ModelYear { get; set; }
        public decimal ListPrice { get; set; }

        public Brand Brand { get; set; }
        public Category Category { get; set; }


    }

    public partial class Product
    {
        public string ProductName { get; set; }
        //public int ProductBrandId { get; set; }
        //public int ProductCategoryId { get; set; }
        //public short ProductModelYear { get; set; }
        //public decimal ProductListPrice { get; set; }
    }

}
