﻿using Models;
using System;
using System.Collections.Generic;

namespace Entities.DAL
{
    public class Categories : IEntity
    {
        public Categories()
        {
            Products = new HashSet<Products>();
        }

        public int CategoryId { get; set; }
        public string CategoryName { get; set; }

        public virtual ICollection<Products> Products { get; set; }
    }
}
