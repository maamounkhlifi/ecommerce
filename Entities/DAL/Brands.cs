﻿using Models;
using System;
using System.Collections.Generic;

namespace Entities.DAL
{
    public class Brands : IEntity
    {
        public Brands()
        {
            Products = new HashSet<Products>();
        }

        public int BrandId { get; set; }
        public string BrandName { get; set; }

        public virtual ICollection<Products> Products { get; set; }
    }
}
