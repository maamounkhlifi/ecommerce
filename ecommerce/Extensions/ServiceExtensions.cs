﻿using Autofac;
using Contracts.Repositories;
using Contracts.Services;
using Entities.DAL;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Repositories;
using Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ecommerce.Extensions
{
    public static class ServiceExtensions
    {
        public static void ConfigureCors(this IServiceCollection services)
        {
            // Configuring CORS for clients from different domains
            services.AddCors(options =>
            {
                options.AddPolicy("CorsPolicy",
                    builder => builder.AllowAnyOrigin()
                    .AllowAnyMethod()
                    .AllowAnyHeader()
                    .WithOrigins("http://localhost:56226")
                    .AllowCredentials());
            });
        }

        public static void InjectServices(this ContainerBuilder containerBuilder)
        {
            containerBuilder.RegisterType<BrandsService>().AsImplementedInterfaces().InstancePerLifetimeScope ();
            containerBuilder.RegisterType<CategoriesService>().AsImplementedInterfaces().InstancePerLifetimeScope ();
            containerBuilder.RegisterType<CustomersService>().AsImplementedInterfaces().InstancePerLifetimeScope ();
            containerBuilder.RegisterType<OrderItemsService>().AsImplementedInterfaces().InstancePerLifetimeScope ();
            containerBuilder.RegisterType<OrdersService>().AsImplementedInterfaces().InstancePerLifetimeScope ();
            containerBuilder.RegisterType<ProductsService>().AsImplementedInterfaces().InstancePerLifetimeScope ();
            containerBuilder.RegisterType<StaffsService>().AsImplementedInterfaces().InstancePerLifetimeScope ();
            containerBuilder.RegisterType<StocksService>().AsImplementedInterfaces().InstancePerLifetimeScope ();
            containerBuilder.RegisterType<StoresService>().AsImplementedInterfaces().InstancePerLifetimeScope ();

        }

        public static void InjectRepositories(this ContainerBuilder containerBuilder)
        {
            containerBuilder.RegisterType<BrandsRepository>().AsImplementedInterfaces().InstancePerLifetimeScope (); ;
            containerBuilder.RegisterType<CategoriesRepository>().AsImplementedInterfaces().InstancePerLifetimeScope (); ;
            containerBuilder.RegisterType<CustomersRepository>().AsImplementedInterfaces().InstancePerLifetimeScope (); ;
            containerBuilder.RegisterType<OrderItemsRepository>().AsImplementedInterfaces().InstancePerLifetimeScope (); ;
            containerBuilder.RegisterType<OrdersRepository>().AsImplementedInterfaces().InstancePerLifetimeScope (); ;
            containerBuilder.RegisterType<ProductsRepository>().AsImplementedInterfaces().InstancePerLifetimeScope (); ;
            containerBuilder.RegisterType<StaffsRepository>().AsImplementedInterfaces().InstancePerLifetimeScope (); ;
            containerBuilder.RegisterType<StocksRepository>().AsImplementedInterfaces().InstancePerLifetimeScope (); ;
            containerBuilder.RegisterType<StoresRepository>().AsImplementedInterfaces().InstancePerLifetimeScope (); ;
        }
        public static void ConfigureDBContext(this IServiceCollection services, IConfiguration config)
        {
            var connectionString = config.GetConnectionString("DefaultConnection");
            services.AddDbContext<ecommerceContext>(options => options.UseSqlServer(connectionString));
        }
    }
}
