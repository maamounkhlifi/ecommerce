﻿using AutoMapper;
using Entities.DAL;
using Entities.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ecommerce.AutoMapperProfiles
{
    public class EcommerceProfile : Profile
    {
        public EcommerceProfile()
        {
            MapBrandsDTO();
            MapCategoriesDTO();
            MapCustomersDTO();
            MapOrderItemsDTO();
            MapOrdersDTO();
            MapProductsDTO();
            MapStaffsDTO();
            MapStocksDTO();
            MapStoreDTO();
        }

        private void MapBrandsDTO()
        {
            CreateMap<Brands, BrandsDTO>()
                .ReverseMap();
        }

        private void MapCategoriesDTO()
        {
            CreateMap<Categories, CategoriesDTO>()
                .ReverseMap();
        }

        private void MapCustomersDTO()
        {
            CreateMap<Customers, CustomersDTO>()
                .ReverseMap();
        }

        private void MapOrderItemsDTO()
        {
            CreateMap<OrderItems, OrderItemsDTO>()
                .ForMember(dest => dest.Order, act => act.MapFrom(
                    src => new Order()
                    {
                        OrderStatus = src.Order.OrderStatus,
                        OrderDate = src.Order.OrderDate,
                        RequiredDate = src.Order.RequiredDate,
                        ShippedDate = src.Order.ShippedDate
                    }))
                .ForMember(dest => dest.Product, act => act.MapFrom(
                    src => new Product()
                    {
                        ProductName = src.Product.ProductName,
                    }))
                .ReverseMap()
                    .ForMember(dest => dest.Order, act => act.Ignore())
                    .ForMember(dest => dest.Product, act => act.Ignore());            
        }

        private void MapOrdersDTO()
        {
            CreateMap<Orders, OrdersDTO>()
                .ForMember(dest => dest.Customer, act => act.MapFrom(
                    src => new Customer()
                    {
                        FirstName = src.Customer.FirstName,
                        LastName = src.Customer.LastName,
                        Phone = src.Customer.Phone,
                        Email = src.Customer.Email
                    }))
                 .ForMember(dest => dest.Staff, act => act.MapFrom(
                    src => new Staff()
                    {
                        FirstName = src.Staff.FirstName,
                        LastName = src.Staff.LastName,
                        Phone = src.Staff.Phone,
                        Email = src.Staff.Email
                    }))
                 .ForMember(dest => dest.Store, act => act.MapFrom(
                    src => new Store()
                    {
                        StoreName = src.Store.StoreName,
                        Phone = src.Store.Phone,
                        Email = src.Store.Email,
                    }))
                 .ReverseMap()
                     .ForMember(dest => dest.Customer, act => act.Ignore())
                     .ForMember(dest => dest.Staff, act => act.Ignore())
                     .ForMember(dest => dest.Store, act => act.Ignore());
        }

        private void MapProductsDTO()
        {
            CreateMap<Products, ProductsDTO>()
               .ForMember(dest => dest.Brand, act => act.MapFrom(
                    src => new Brand()
                    {
                        BrandName = src.Brand.BrandName
                    }))
               .ForMember(dest => dest.Category, act => act.MapFrom(
                    src => new Category()
                    {
                        CategoryName = src.Category.CategoryName
                    }))
               .ReverseMap()
                   .ForMember(dest => dest.Brand, act => act.Ignore())
                   .ForMember(dest => dest.Category, act => act.Ignore());
        }

        private void MapStaffsDTO()
        {
            CreateMap<Staffs, StaffsDTO>()
                .ForMember(dest => dest.Manager, act => act.MapFrom(
                    src => new Staff()
                    {
                        FirstName = src.Manager.FirstName,
                        LastName = src.Manager.LastName,
                        Phone = src.Manager.Phone,
                        Email = src.Manager.Email
                    }))
                 .ForMember(dest => dest.Store, act => act.MapFrom(
                    src => new Store()
                    {
                        StoreName = src.Store.StoreName,
                        Phone = src.Store.Phone,
                        Email = src.Store.Email,
                    }))
                 .ReverseMap()
                     .ForMember(dest => dest.Manager, act => act.Ignore())
                     .ForMember(dest => dest.Store, act => act.Ignore());
        }

        private void MapStocksDTO()
        {
            CreateMap<Stocks, StocksDTO>()
                .ForMember(dest => dest.Product, act => act.MapFrom(
                    src => new Product()
                    {
                        ProductName = src.Product.ProductName,
                    }))
                .ForMember(dest => dest.Store, act => act.MapFrom(
                    src => new Store()
                    {
                        StoreName = src.Store.StoreName,
                        Phone = src.Store.Phone,
                        Email = src.Store.Email,
                    }))
                .ReverseMap()
                    .ForMember(dest => dest.Product, act => act.Ignore())
                    .ForMember(dest => dest.Store, act => act.Ignore());
        }

        private void MapStoreDTO()
        {
            CreateMap<Stores, StoresDTO>()
                .ReverseMap();
        }
    }
}
