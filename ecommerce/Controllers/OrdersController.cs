﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Contracts.Services;
using Entities.DAL;
using Entities.DTO;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace ecommerce.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class OrdersController : Controller
    {

        private IOrdersService _ordersService;

        public OrdersController(IOrdersService ordersService)
        {            
            _ordersService = ordersService;
        }

        // GET: api/Orders
        [HttpGet]
        public ActionResult<IEnumerable<OrdersDTO>> GetOrder()
        {
            var result = _ordersService.FindAllAsync();
            return Ok(result);
        }

        // GET: api/Orders/5
        [HttpGet("{id}")]
        public ActionResult<OrdersDTO> GetOrder(int id)
        {
            var order = _ordersService.FindByConditionAsync(p => p.OrderId == id, pr => pr.Customer, pr => pr.Store, pr => pr.Staff).FirstOrDefault();
            return Ok(order);
        }

        // PUT: api/Orders/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutOrder(int id, OrdersDTO orderDTO)
        {
            if (id != orderDTO.OrderId)
            {
                return BadRequest();
            }

            _ordersService.Update(orderDTO);

            try
            {
                await _ordersService.Save();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!OrderExists(id).Result)
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Orders
        [HttpPost]
        public async Task<ActionResult<OrdersDTO>> PostOrder(OrdersDTO orderDTO)
        {
            _ordersService.Create(orderDTO);
            await _ordersService.Save();

            return CreatedAtAction("PostOrder", new { id = orderDTO.OrderId }, orderDTO);
        }

        // DELETE: api/Orders/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<OrdersDTO>> DeleteOrder(int id)
        {
            var orderDTO = _ordersService.FindByConditionAsync(p => p.OrderId == id);
            if (orderDTO.Count() == 0)
            {
                return NotFound();
            }

            _ordersService.Delete(orderDTO.First());
            await _ordersService.Save();

            return orderDTO.First();
        }        

        private async Task<bool> OrderExists(int id)
        {
            return await _ordersService.Exists(id);
        }
    }
}