﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Contracts.Services;
using Entities.DAL;
using Entities.DTO;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace ecommerce.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class CategoriesController : Controller
    {

        private ICategoriesService _categoriesService;

        public CategoriesController(ICategoriesService categoriesService)
        {            
            _categoriesService = categoriesService;
        }

        // GET: api/Categories
        [HttpGet]
        public ActionResult<IEnumerable<CategoriesDTO>> GetCategory()
        {
            var result = _categoriesService.FindAllAsync();
            return Ok(result);
        }

        // GET: api/Categories/5
        [HttpGet("{id}")]
        public ActionResult<CategoriesDTO> GetCategory(int id)
        {
            var category = _categoriesService.FindByConditionAsync(p => p.CategoryId == id).FirstOrDefault();
            return Ok(category);
        }

        // PUT: api/Categories/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCategory(int id, CategoriesDTO categoryDTO)
        {
            if (id != categoryDTO.CategoryId)
            {
                return BadRequest();
            }

            _categoriesService.Update(categoryDTO);

            try
            {
                await _categoriesService.Save();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CategoryExists(id).Result)
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Categories
        [HttpPost]
        public async Task<ActionResult<CategoriesDTO>> PostCategory(CategoriesDTO categoryDTO)
        {
            _categoriesService.Create(categoryDTO);
            await _categoriesService.Save();

            return CreatedAtAction("PostCategory", new { id = categoryDTO.CategoryId }, categoryDTO);
        }

        // DELETE: api/Categories/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<CategoriesDTO>> DeleteCategory(int id)
        {
            var categoryDTO = _categoriesService.FindByConditionAsync(p => p.CategoryId == id);
            if (categoryDTO.Count() == 0)
            {
                return NotFound();
            }

            _categoriesService.Delete(categoryDTO.First());
            await _categoriesService.Save();

            return categoryDTO.First();
        }        

        private async Task<bool> CategoryExists(int id)
        {
            return await _categoriesService.Exists(id);
        }
    }
}