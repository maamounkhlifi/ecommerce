﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Contracts.Services;
using Entities.DAL;
using Entities.DTO;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace ecommerce.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class CustomersController : Controller
    {

        private ICustomersService _customersService;

        public CustomersController(ICustomersService customersService)
        {            
            _customersService = customersService;
        }

        // GET: api/Customers
        [HttpGet]
        public ActionResult<IEnumerable<CustomersDTO>> GetCustomer()
        {
            var result = _customersService.FindAllAsync();
            return Ok(result);
        }

        // GET: api/Customers/5
        [HttpGet("{id}")]
        public ActionResult<CustomersDTO> GetCustomer(int id)
        {
            var customer = _customersService.FindByConditionAsync(p => p.CustomerId == id).FirstOrDefault();
            return Ok(customer);
        }

        // PUT: api/Customers/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCustomer(int id, CustomersDTO customerDTO)
        {
            if (id != customerDTO.CustomerId)
            {
                return BadRequest();
            }

            _customersService.Update(customerDTO);

            try
            {
                await _customersService.Save();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CustomerExists(id).Result)
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Customers
        [HttpPost]
        public async Task<ActionResult<CustomersDTO>> PostCustomer(CustomersDTO customerDTO)
        {
            _customersService.Create(customerDTO);
            await _customersService.Save();

            return CreatedAtAction("PostCustomer", new { id = customerDTO.CustomerId }, customerDTO);
        }

        // DELETE: api/Customers/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<CustomersDTO>> DeleteCustomer(int id)
        {
            var customerDTO = _customersService.FindByConditionAsync(p => p.CustomerId == id);
            if (customerDTO.Count() == 0)
            {
                return NotFound();
            }

            _customersService.Delete(customerDTO.First());
            await _customersService.Save();

            return customerDTO.First();
        }        

        private async Task<bool> CustomerExists(int id)
        {
            return await _customersService.Exists(id);
        }
    }
}