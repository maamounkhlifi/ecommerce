﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Contracts.Services;
using Entities.DAL;
using Entities.DTO;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace ecommerce.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class StocksController : Controller
    {

        private IStocksService _stocksService;

        public StocksController(IStocksService stocksService)
        {            
            _stocksService = stocksService;
        }

        // GET: api/Stocks
        [HttpGet]
        public ActionResult<IEnumerable<StocksDTO>> GetStock()
        {
            var result = _stocksService.FindAllAsync();
            return Ok(result);
        }

        // GET: api/Stocks/5/3
        [HttpGet("{productID}/{storeId}")]
        public ActionResult<StocksDTO> GetStock(int productID, int storeId)
        {
            var stock = _stocksService.FindByConditionAsync(p => p.ProductId == productID && p.StoreId == storeId, pr => pr.Product, pr => pr.Store).FirstOrDefault();
            return Ok(stock);
        }

        // PUT: api/Stocks/5/3
        [HttpPut("{productID}/{storeId}")]
        public async Task<IActionResult> PutStock(int productID, int storeId, StocksDTO stockDTO)
        {
            if (productID != stockDTO.ProductId || stockDTO.StoreId != storeId)
            {
                return BadRequest();
            }

            _stocksService.Update(stockDTO);

            try
            {
                await _stocksService.Save();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!StockExists(productID, storeId).Result)
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Stocks
        [HttpPost]
        public async Task<ActionResult<StocksDTO>> PostStock(StocksDTO stockDTO)
        {
            _stocksService.Create(stockDTO);
            await _stocksService.Save();

            return CreatedAtAction("PostStock", new { productID = stockDTO.ProductId, storeId = stockDTO.StoreId }, stockDTO);
        }

        // DELETE: api/Stocks/5/3
        [HttpDelete("{productID}/{storeId}")]
        public async Task<ActionResult<StocksDTO>> DeleteStock(int productID, int storeId)
        {
            var stockDTO = _stocksService.FindByConditionAsync(p => p.ProductId == productID && p.StoreId == storeId);
            if (stockDTO.Count() == 0)
            {
                return NotFound();
            }

            _stocksService.Delete(stockDTO.First());
            await _stocksService.Save();

            return stockDTO.First();
        }        

        private async Task<bool> StockExists(int productID, int storeId)
        {
            return await _stocksService.Exists(productID, storeId);
        }
    }
}