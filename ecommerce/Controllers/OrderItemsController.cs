﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Contracts.Services;
using Entities.DAL;
using Entities.DTO;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace ecommerce.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class OrderItemsController : Controller
    {

        private IOrderItemsService _orderItemsService;

        public OrderItemsController(IOrderItemsService orderItemsService)
        {            
            _orderItemsService = orderItemsService;
        }

        // GET: api/OrderItems
        [HttpGet]
        public ActionResult<IEnumerable<OrderItemsDTO>> GetOrderItem()
        {
            var result = _orderItemsService.FindAllAsync();
            return Ok(result);
        }

        // GET: api/OrderItems/5/3
        [HttpGet("{orderId}/{itemId}")]
        public ActionResult<OrderItemsDTO> GetOrderItem(int orderId, int itemId)
        {
            var orderItem = _orderItemsService.FindByConditionAsync(p => p.OrderId == orderId && p.ItemId == itemId, pr => pr.Order, pr => pr.Product).FirstOrDefault();
            return Ok(orderItem);
        }

        // PUT: api/OrderItems/5/3
        [HttpPut("{orderId}/{itemId}")]
        public async Task<IActionResult> PutOrderItem(int orderId, int itemId, OrderItemsDTO orderItemDTO)
        {
            if (orderId != orderItemDTO.OrderId || itemId != orderItemDTO.ItemId)
            {
                return BadRequest();
            }

            _orderItemsService.Update(orderItemDTO);

            try
            {
                await _orderItemsService.Save();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!OrderItemExists(orderId, itemId).Result)
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/OrderItems
        [HttpPost]
        public async Task<ActionResult<OrderItemsDTO>> PostOrderItem(OrderItemsDTO orderItemDTO)
        {
            _orderItemsService.Create(orderItemDTO);
            await _orderItemsService.Save();

            return CreatedAtAction("PostOrderItem", new { orderId = orderItemDTO.OrderId, itemID = orderItemDTO.ItemId }, orderItemDTO);
        }

        // DELETE: api/OrderItems/5/3
        [HttpDelete("{orderId}/{itemId}")]
        public async Task<ActionResult<OrderItemsDTO>> DeleteOrderItem(int orderId, int itemId)
        {
            var orderItemDTO = _orderItemsService.FindByConditionAsync(p => p.OrderId == orderId && p.ItemId == itemId);
            if (orderItemDTO.Count() == 0)
            {
                return NotFound();
            }

            _orderItemsService.Delete(orderItemDTO.First());
            await _orderItemsService.Save();

            return orderItemDTO.First();
        }        

        private async Task<bool> OrderItemExists(int orderId, int itemId)
        {
            return await _orderItemsService.Exists(orderId, itemId);
        }
    }
}