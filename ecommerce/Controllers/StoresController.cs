﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Contracts.Services;
using Entities.DAL;
using Entities.DTO;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace ecommerce.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class StoresController : Controller
    {

        private IStoresService _storesService;

        public StoresController(IStoresService storesService)
        {            
            _storesService = storesService;
        }

        // GET: api/Stores
        [HttpGet]
        public ActionResult<IEnumerable<StoresDTO>> GetStore()
        {
            var result = _storesService.FindAllAsync();
            return Ok(result);
        }

        // GET: api/Stores/5
        [HttpGet("{id}")]
        public ActionResult<StoresDTO> GetStore(int id)
        {
            var store = _storesService.FindByConditionAsync(p => p.StoreId == id).FirstOrDefault();
            return Ok(store);
        }

        // PUT: api/Stores/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutStore(int id, StoresDTO storeDTO)
        {
            if (id != storeDTO.StoreId)
            {
                return BadRequest();
            }

            _storesService.Update(storeDTO);

            try
            {
                await _storesService.Save();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!StoreExists(id).Result)
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Stores
        [HttpPost]
        public async Task<ActionResult<StoresDTO>> PostStore(StoresDTO storeDTO)
        {
            _storesService.Create(storeDTO);
            await _storesService.Save();

            return CreatedAtAction("PostStore", new { id = storeDTO.StoreId }, storeDTO);
        }

        // DELETE: api/Stores/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<StoresDTO>> DeleteStore(int id)
        {
            var storeDTO = _storesService.FindByConditionAsync(p => p.StoreId == id);
            if (storeDTO.Count() == 0)
            {
                return NotFound();
            }

            _storesService.Delete(storeDTO.First());
            await _storesService.Save();

            return storeDTO.First();
        }        

        private async Task<bool> StoreExists(int id)
        {
            return await _storesService.Exists(id);
        }
    }
}