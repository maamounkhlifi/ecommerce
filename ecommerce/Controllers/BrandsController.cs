﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Contracts.Services;
using Entities.DAL;
using Entities.DTO;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace ecommerce.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class BrandsController : Controller
    {

        private IBrandsService _brandsService;

        public BrandsController(IBrandsService brandsService)
        {            
            _brandsService = brandsService;
        }

        // GET: api/Brands
        [HttpGet]
        public ActionResult<IEnumerable<BrandsDTO>> GetBrand()
        {
            var result = _brandsService.FindAllAsync();
            return Ok(result);
        }

        // GET: api/Brands/5
        [HttpGet("{id}")]
        public ActionResult<BrandsDTO> GetBrand(int id)
        {
            var brand = _brandsService.FindByConditionAsync(p => p.BrandId == id).FirstOrDefault();
            return Ok(brand);
        }

        // PUT: api/Brands/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutBrand(int id, BrandsDTO brandDTO)
        {
            if (id != brandDTO.BrandId)
            {
                return BadRequest();
            }

            _brandsService.Update(brandDTO);

            try
            {
                await _brandsService.Save();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!BrandExists(id).Result)
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Brands
        [HttpPost]
        public async Task<ActionResult<BrandsDTO>> PostBrand(BrandsDTO brandDTO)
        {
            _brandsService.Create(brandDTO);
            await _brandsService.Save();

            return CreatedAtAction("PostBrand", new { id = brandDTO.BrandId }, brandDTO);
        }

        // DELETE: api/Brands/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<BrandsDTO>> DeleteBrand(int id)
        {
            var brandDTO = _brandsService.FindByConditionAsync(p => p.BrandId == id);
            if (brandDTO.Count() == 0)
            {
                return NotFound();
            }

            _brandsService.Delete(brandDTO.First());
            await _brandsService.Save();

            return brandDTO.First();
        }        

        private async Task<bool> BrandExists(int id)
        {
            return await _brandsService.Exists(id);
        }
    }
}