﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Contracts.Services;
using Entities.DAL;
using Entities.DTO;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace ecommerce.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ProductsController : Controller
    {

        private IProductsService _productsService;

        public ProductsController(IProductsService productsService)
        {            
            _productsService = productsService;
        }

        // GET: api/Products
        [HttpGet]
        public ActionResult<IEnumerable<ProductsDTO>> GetProduct()
        {
            var result = _productsService.FindAllAsync();
            return Ok(result);
        }

        // GET: api/Products/5
        [HttpGet("{id}")]
        public ActionResult<ProductsDTO> GetProduct(int id)
        {
            var product = _productsService.FindByConditionAsync(p => p.ProductId == id, pr => pr.Brand, pr => pr.Category).FirstOrDefault();
            return Ok(product);
        }

        // PUT: api/Products/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutProduct(int id, ProductsDTO productDTO)
        {
            if (id != productDTO.ProductId)
            {
                return BadRequest();
            }

            _productsService.Update(productDTO);

            try
            {
                await _productsService.Save();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ProductExists(id).Result)
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Products
        [HttpPost]
        public async Task<ActionResult<ProductsDTO>> PostProduct(ProductsDTO productDTO)
        {
            _productsService.Create(productDTO);
            await _productsService.Save();

            return CreatedAtAction("PostProduct", new { id = productDTO.ProductId }, productDTO);
        }

        // DELETE: api/Products/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<ProductsDTO>> DeleteProduct(int id)
        {
            var productDTO = _productsService.FindByConditionAsync(p => p.ProductId == id);
            if (productDTO.Count() == 0)
            {
                return NotFound();
            }

            _productsService.Delete(productDTO.First());
            await _productsService.Save();

            return productDTO.First();
        }        

        private async Task<bool> ProductExists(int id)
        {
            return await _productsService.Exists(id);
        }
    }
}