﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Contracts.Services;
using Entities.DAL;
using Entities.DTO;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace ecommerce.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class StaffsController : Controller
    {

        private IStaffsService _staffsService;

        public StaffsController(IStaffsService staffsService)
        {            
            _staffsService = staffsService;
        }

        // GET: api/Staffs
        [HttpGet]
        public ActionResult<IEnumerable<StaffsDTO>> GetStaff()
        {
            var result = _staffsService.FindAllAsync();
            return Ok(result);
        }

        // GET: api/Staffs/5
        [HttpGet("{id}")]
        public ActionResult<StaffsDTO> GetStaff(int id)
        {
            var staff = _staffsService.FindByConditionAsync(p => p.StaffId == id, pr => pr.Manager, pr => pr.Store).FirstOrDefault();
            return Ok(staff);
        }

        // PUT: api/Staffs/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutStaff(int id, StaffsDTO staffDTO)
        {
            if (id != staffDTO.StaffId)
            {
                return BadRequest();
            }

            _staffsService.Update(staffDTO);

            try
            {
                await _staffsService.Save();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!StaffExists(id).Result)
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Staffs
        [HttpPost]
        public async Task<ActionResult<StaffsDTO>> PostStaff(StaffsDTO staffDTO)
        {
            _staffsService.Create(staffDTO);
            await _staffsService.Save();

            return CreatedAtAction("PostStaff", new { id = staffDTO.StaffId }, staffDTO);
        }

        // DELETE: api/Staffs/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<StaffsDTO>> DeleteStaff(int id)
        {
            var staffDTO = _staffsService.FindByConditionAsync(p => p.StaffId == id);
            if (staffDTO.Count() == 0)
            {
                return NotFound();
            }

            _staffsService.Delete(staffDTO.First());
            await _staffsService.Save();

            return staffDTO.First();
        }        

        private async Task<bool> StaffExists(int id)
        {
            return await _staffsService.Exists(id);
        }
    }
}