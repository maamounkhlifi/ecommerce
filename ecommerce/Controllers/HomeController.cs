﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ecommerce.Controllers
{
    public class HomeController : Controller
    {
        public HomeController()
        {            
        }
        public IActionResult Index()
        {
            return Ok("Hello World");
        }        
    }
}
