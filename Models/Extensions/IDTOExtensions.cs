﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.Extensions
{
    public static class IDTOExtensions
    {
        public static bool HasValue(this IDTO DTO)
        {
            return DTO != null;
        }
    }
}
