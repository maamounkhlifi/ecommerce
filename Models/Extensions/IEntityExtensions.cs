﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.Extensions
{
    public static class IEntityExtensions
    {
        public static bool HasValue(this IEntity entity)
        {
            return entity != null;
        }
    }
}
