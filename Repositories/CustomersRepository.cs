﻿using Contracts.Repositories;
using Entities.DAL;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Repositories
{
    public class CustomersRepository : RepositoryBase<Customers>, ICustomersRepository
    {
        public CustomersRepository(ecommerceContext ecommerceContext)
            : base(ecommerceContext)
        {
        }

        public Task<bool> Exists(int id)
        {
            return this.ecommerceContext.Customers.AnyAsync(p => p.CustomerId == id);
        }
    }
}
