﻿using Contracts.Repositories;
using Entities.DAL;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Repositories
{
    public class RepositoryBase<T> : IRepositoryBase<T> where T : class
    {
        protected ecommerceContext ecommerceContext { get; set; }

        public RepositoryBase(ecommerceContext ecommerceContext)
        {
            this.ecommerceContext = ecommerceContext;
        }

        public IQueryable<T> FindAll()
        {
            return this.ecommerceContext.Set<T>().AsNoTracking();
        }

        public async Task<IEnumerable<T>> FindAllAsync()
        {
            return await this.ecommerceContext.Set<T>().AsNoTracking().ToListAsync<T>();
        }

        public IQueryable<T> FindByCondition(Expression<Func<T, bool>> expression)
        {
            return this.ecommerceContext.Set<T>().AsNoTracking().Where(expression).AsNoTracking();
        }

        public async Task<IEnumerable<T>> FindByConditionAsync(Expression<Func<T, bool>> expression)
        {
            return await this.ecommerceContext.Set<T>().AsNoTracking().Where(expression).ToListAsync();
        }

        public async Task<IEnumerable<T>> FindByConditionAsync(Expression<Func<T, bool>> expression, params Expression<Func<T, object>>[] includeExpressions)
        {
            IQueryable<T> dbSet = this.ecommerceContext.Set<T>();
            foreach (Expression<Func<T, object>> includeExpression in includeExpressions)
            {
                dbSet = dbSet.Include(includeExpression);
            }
            return await dbSet.AsNoTracking().Where(expression).ToListAsync();
        }

        public void Create(T entity)
        {
            this.ecommerceContext.Set<T>().Add(entity);
        }

        public void Update(T entity)
        {
            this.ecommerceContext.Set<T>().Update(entity);
        }

        public void Delete(T entity)
        {
            this.ecommerceContext.Set<T>().Remove(entity);
        }
        public async Task<int> Count()
        {
            return await this.ecommerceContext.Set<T>().CountAsync();
        }       
    }
}
