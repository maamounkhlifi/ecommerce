﻿using Contracts.Repositories;
using Entities.DAL;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Repositories
{
    public class BrandsRepository : RepositoryBase<Brands>, IBrandsRepository
    {
        public BrandsRepository(ecommerceContext ecommerceContext)
            : base(ecommerceContext)
        {
        }

        public Task<bool> Exists(int id)
        {
            return this.ecommerceContext.Brands.AnyAsync(p => p.BrandId == id);
        }
    }
}
