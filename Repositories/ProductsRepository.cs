﻿using Contracts.Repositories;
using Entities.DAL;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Repositories
{
    public class ProductsRepository : RepositoryBase<Products>, IProductsRepository
    {
        public ProductsRepository(ecommerceContext ecommerceContext)
            : base(ecommerceContext)
        {
        }

        public Task<bool> Exists(int id)
        {
            return this.ecommerceContext.Products.AnyAsync(p => p.ProductId == id);
        }
    }
}
