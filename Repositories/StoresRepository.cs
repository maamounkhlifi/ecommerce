﻿using Contracts.Repositories;
using Entities.DAL;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Repositories
{
    public class StoresRepository : RepositoryBase<Stores>, IStoresRepository
    {
        public StoresRepository(ecommerceContext ecommerceContext)
            : base(ecommerceContext)
        {
        }

        public Task<bool> Exists(int id)
        {
            return this.ecommerceContext.Stores.AnyAsync(p => p.StoreId == id);
        }
    }
}
