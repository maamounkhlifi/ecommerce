﻿using Contracts.Repositories;
using Entities.DAL;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Repositories
{
    public class OrdersRepository : RepositoryBase<Orders>, IOrdersRepository
    {
        public OrdersRepository(ecommerceContext ecommerceContext)
            : base(ecommerceContext)
        {
        }
        public Task<bool> Exists(int id)
        {
            return this.ecommerceContext.Orders.AnyAsync(p => p.OrderId == id);
        }
    }
}
