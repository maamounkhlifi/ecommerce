﻿using Contracts.Repositories;
using Entities.DAL;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Repositories
{
    public class CategoriesRepository : RepositoryBase<Categories>, ICategoriesRepository
    {
        public CategoriesRepository(ecommerceContext ecommerceContext)
            : base(ecommerceContext)
        {
        }
        public Task<bool> Exists(int id)
        {
            return this.ecommerceContext.Categories.AnyAsync(p => p.CategoryId == id);
        }
    }
}
