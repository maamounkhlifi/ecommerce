﻿using Contracts.Repositories;
using Entities.DAL;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Repositories
{
    public class OrderItemsRepository : RepositoryBase<OrderItems>, IOrderItemsRepository
    {
        public OrderItemsRepository(ecommerceContext ecommerceContext)
            : base(ecommerceContext)
        {
        }

        public Task<bool> Exists(int orderId, int ItemId)
        {
            return this.ecommerceContext.OrderItems.AnyAsync(p => p.OrderId == orderId && p.ItemId == ItemId);
        }
    }
}
