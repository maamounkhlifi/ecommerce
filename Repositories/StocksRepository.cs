﻿using Contracts.Repositories;
using Entities.DAL;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Repositories
{
    public class StocksRepository : RepositoryBase<Stocks>, IStocksRepository
    {
        public StocksRepository(ecommerceContext ecommerceContext)
            : base(ecommerceContext)
        {
        }
        public Task<bool> Exists(int productId, int storeId)
        {
            return this.ecommerceContext.Stocks.AnyAsync(p => p.ProductId == productId && p.StoreId == storeId);
        }
    }
}
