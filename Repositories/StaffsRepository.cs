﻿using Contracts.Repositories;
using Entities.DAL;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Repositories
{
    public class StaffsRepository : RepositoryBase<Staffs>, IStaffsRepository
    {
        public StaffsRepository(ecommerceContext ecommerceContext)
            : base(ecommerceContext)
        {
        }

        public Task<bool> Exists(int id)
        {
            return this.ecommerceContext.Staffs.AnyAsync(p => p.StaffId == id);
        }
    }
}
